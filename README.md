Basic Style Guide
=========

A simple guide to help show different elements of a site. This is a blank SASS sheet so you can drop your own styling in to see how it will look.

## Installation
* Download the zip and unzip.
* Open index.html or start editing the SASS

## Requirements
* This uses [Compass](http://compass-style.org/) so make sure to have it installed.

## Features
* Grid uses the [Toast Framework](http://daneden.github.io/Toast/)
* Include multiple reset sheets that can be turned on or off: Normalize, Yahoo Reset, Blueprint Reset (Blueprint needs to be installed)
* Visual color grid for easy implementation
* "Kitchen Sink" Typography for easy styling
* Form and table examples
